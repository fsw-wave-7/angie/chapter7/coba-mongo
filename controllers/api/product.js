const client = require("../../config/connection")
const db = client.db("mongo")
const ObjectId = require("mongodb").ObjectId

class ProductController {
    getProducts = async (req, res) => {
        const products = await db.collection("products").find().toArray()
        res.send(products);
    }

    postProducts = async (req, res) => {
        const {
            name,
            price,
            stock
        } = req.body;
        const products = await db.collection("products").insertOne({
            name: name,
            price: price,
            stock: stock
        });
        res.send({
            status: "sukses",
            message: "Produk berhasil ditambahkan",
            data: products.ops
        })
    }

    getDetailProduct = async (req, res) => {
        const products = await db.collection("products").findOne({
            _id: ObjectId(req.params.index)
        })
        res.send(products);
    }

    updateProduct = async (req, res) => {
        const {
            name,
            price,
            stock
        } = req.body;

        const products = await db.collection("products").updateOne({
            _id: ObjectId(req.params.index)

        }, { $set: { name, price, stock } })
        res.send(products);
    }

    deleteProduct = async (req, res) => {
        const products = await db.collection("products").deleteOne({
            _id: ObjectId(req.params.index)
        })
        res.send("berhasil menghapus")
    }
}

module.exports = ProductController