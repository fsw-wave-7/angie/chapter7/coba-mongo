const api = require('express').Router()
const ProductController = require("../controllers/api/product");
const productController = new ProductController()

api.get("/products", productController.getProducts);   
api.post("/products", productController.postProducts); 
api.get("/product/:index", productController.getDetailProduct); 
api.put("/product/:index", productController.updateProduct);
api.delete("/product/:index", productController.deleteProduct);

module.exports = api     