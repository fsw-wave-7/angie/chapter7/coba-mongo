const { Router } = require('express');
const express = require('express');
// call routing from express
const app = express();
const port = 3000
const multer = require('multer')
const parser = multer().none()

// get data json from postman 
app.use(express.json());

// parsing form data 
app.use(parser)

// get data from urlencoded
app.use(express.urlencoded({extended: false}));

// call api route
const api = require("./routes/api")
app.use("/api", api)

//error handling
app.use((req,res) => {
    res.status(404).send("HALAMAN TIDAK DITEMUKAN")
})

// 500 error handling 
app.use((err, req, res) => {
    res.status(500).send("INTERNAL SERVER ERROR")
})

app.listen(port, () => {
    console.log(`server running at http://localhost:${port}`);
})